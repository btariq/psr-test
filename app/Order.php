<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Order Items of an Order.
     */
    public function OrderItems()
    {
        return $this->hasMany('App\OrderItems', 'id','order_id');
    }

    /**
     * Order Belong to Customer.
     */
    public function Customer()
    {
        return $this->belongsTo('App\Customer');
    }
}