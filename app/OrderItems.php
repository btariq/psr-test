<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    /**
     * OrderItems Belong to Order.
     */
    public function Order()
    {
        return $this->belongsTo('App\Order');
    }


    /**
     * OrderItems Belong to Order.
     */
    public function Product()
    {
        return $this->belongsTo('App\Product');
    }
}