@extends('layouts.app')
@section('content')
@if ($user->isAn('admin') || $user->isAn('shop'))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header">{{ $title }}</div>
            <div class="card-body">
            <div id="dialog-form" title="Order Items Details">
            <table>
            <thead>
                        <tr>
                            <th>Invoice number</th>
                            <th>Order Id</th>
                            <th>Product Name</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    
                    <tfoot>
                        <tr>
                            <th>Invoice number</th>
                            <th>Order Id</th>
                            <th>Product Name</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                        </tr>
                    </tfoot>
            
                    <tbody id="table-content">
            </tbody>
            </table>
            </div>
            <table id="datatable" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Invoice number</th>
                            <th>Total amount</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
            
                    <tfoot>
                        <tr>
                            <th>Invoice number</th>
                            <th>Total amount</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
            
                    <tbody>
            </tbody>
            </table>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
   <script>
    $(document).ready( function () {
        $.noConflict();
        var customersTable = $('#datatable').DataTable({
            ajax: "{{ route('orders.datatables') }}",
            columns: [
            {data: 'invoice_number'},
            {data: 'total_amount'},
            {data: 'status'},
            {data: 'created_at', searchable: false},
            {   data: "id" , render : function ( data, type, row, meta ) {
              return '<a class="custom-button" href="#" data-id="'+data+'">Details</a>' 
            }}
            ],
            
        });
        $('#DataTables_Table_0_filter label input').focus();
        $( "#dialog-form" ).dialog({
            autoOpen: false,
            maxWidth:600,
                    maxHeight: 500,
            height: 500,
            width: 600,
            modal: true,
            position: top,
            buttons: {
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });          
        $( document ).on( "click", "a.custom-button", function() {
            var orderId = $(this).attr('data-id');
             $.ajax({
                    method: "GET",
                    url: "{{ route('order-items.index') }}",
                    data: { id: orderId }
                })
                .done(function( data ) {
                    $("#table-content").empty();
                     
                    for(var i in data){
                        var str = '<tr><td>'+data[i].order.invoice_number+'</td><td>'+data[i].order.id+'</td><td>'+data[i].product.name+'</td><td>'+data[i].order.customer.name+'</td><td>'+data[i].order.created_at+'</td></tr>';
                        $("#table-content").append(str);
                    }
                });
                $( "#dialog-form" ).dialog( "open" );
        });   
    });
    </script>
@endif
@endsection