<?php

use Illuminate\Database\Seeder;
use App\Order as Order;
use App\Product as Product;
use App\OrderItems as OrderItems;

class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderItems::truncate();
        $faker = Faker\Factory::create();

        $orders = Order::orderBy('id','desc')->get();        
        $orders->each(function($order) use ($faker){
            
            $products = Product::inRandomOrder()->take(2)->get();
            $products->each(function($product) use ($order, $faker){
                OrderItems::insert([
                    'order_id'  =>  $order->id,
                    'product_id'    =>  $product->id,
                    'quantity'  =>   $faker->randomNumber(2),
                    'created_at'    =>  date('Y-m-d H:i:s'),
                    'updated_at'    =>  date('Y-m-d H:i:s')
                ]);
            });
        });
    }
}
