<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Order Items has many products.
     */
    public function OrderItems()
    {
        return $this->hasMany('App\OrderItems', 'id','product_id');
    }

}