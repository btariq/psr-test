<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Bouncer::allow('admin')->to('view', \App\User::class);
        $admin = factory(App\User::class)->create([
            'name' => 'administrator',
            'email' => 'admin@psr.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => bcrypt('test123'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $admin->assign('admin');

        Bouncer::allow('user')->to('view', \App\User::class);
        $admin = factory(App\User::class)->create([
            'name' => 'user-manager',
            'email' => 'user@psr.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => bcrypt('test123'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $admin->assign('user');

        Bouncer::allow('shop')->to('view', \App\User::class);
        $admin = factory(App\User::class)->create([
            'name' => 'shop-manager',
            'email' => 'shop@psr.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => bcrypt('test123'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $admin->assign('shop');
    }
}
