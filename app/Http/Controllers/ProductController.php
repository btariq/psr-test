<?php

namespace App\Http\Controllers;

use Auth;
use App\Product as Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product.index', ['title' => 'Products Data', 'user' => Auth::user()]);
    }

    /**
     * Display a listing of the Product resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        $products = Product::select(['name', 'price', 'in_stock', 'created_at']);

        return datatables($products)
                ->editColumn('created_at', function ($product) {
                    return $product->created_at->format('d/m/Y');
                })
                ->editColumn('in_stock', function ($product) {
                    return 0 == $product->in_stock ? 'Not available' : 'available';
                })
                ->make(true);
    }
}
