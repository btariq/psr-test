<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\OrderItems as OrderItems;

class OrderItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        activity()->log(''.Auth::user()->name.' processed the order: ['.$request->get('id').']');

        return OrderItems::with(['order.customer', 'product'])
                    ->where('order_id', '=', $request->get('id'))
                    ->get();
    }
}
