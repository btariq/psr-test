@extends('layouts.app')

@section('content')

@if ($user->isAn('admin') || $user->isAn('user'))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">{{ $title }}</div>
            <div class="card-body">
            <table id="datatable" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Date</th>
                        </tr>
                    </tfoot>
            
                    <tbody>
            </tbody>
            </table>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
   <script>
    $(document).ready( function () {
        $.noConflict();
        var customersTable = $('#datatable').DataTable({
            ajax: "{{ route('customers.datatables') }}",
            columns: [
            {data: 'name'},
            {data: 'email'},
            {data: 'created_at', searchable: false}
            ]
        });
        $('#DataTables_Table_0_filter label input').focus();
       
    });
    </script>
@endif
@endsection