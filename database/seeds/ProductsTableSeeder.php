<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('products')->truncate();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; ++$i) {
            DB::table('products')->insert([
                'name' => $faker->word,
                'price' => $faker->randomNumber(3),
                'in_stock' => 0 == $i % 2 ? 0 : 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
