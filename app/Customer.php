<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * Customers has many Orders.
     */
    public function Orders()
    {
        return $this->hasMany('App\Order', 'id','customer_id');
    }
}