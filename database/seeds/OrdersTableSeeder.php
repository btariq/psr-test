<?php

use App\Customer as Customer;
use App\Order as Order;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Order::truncate();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 50; ++$i) {
            $customerId = Customer::inRandomOrder()->first();
            Order::insert([
                'invoice_number' => $faker->randomNumber(5),
                'total_amount' => $faker->randomNumber(5),
                'customer_id' => $customerId->id,
                'status' => 0 == $i % 2 ? 'new' : 'processed',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
