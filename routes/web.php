<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ 'as' => 'index','uses' => 'HomeController@index']);

Route::get('product', 'ProductController@index')->name('product.index')->middleware('auth');
Route::get('customer', 'CustomerController@index')->name('customer.index')->middleware('auth');
Route::get('order', 'OrderController@index')->name('order.index')->middleware('auth');
Route::get('order-items', 'OrderItemsController@index')->name('order-items.index')->middleware('auth');

Route::get('products-datatables' ,['as' => 'products.datatables', 'uses'  =>  'ProductController@datatables'])->middleware('auth');
Route::get('customers-datatables' ,['as' => 'customers.datatables', 'uses'  =>  'CustomerController@datatables'])->middleware('auth');
Route::get('orders-datatables' ,['as' => 'orders.datatables', 'uses'  =>  'OrderController@datatables'])->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
