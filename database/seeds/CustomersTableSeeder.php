<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('customers')->truncate();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 200; ++$i) {
            DB::table('customers')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $this->command->info('Inserted all Customers');
    }
}
