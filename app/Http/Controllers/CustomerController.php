<?php

namespace App\Http\Controllers;

use Auth;
use App\Customer as Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.index', ['title' => 'Customers Data', 'user' => Auth::user()]);
    }

    /**
     * Display a listing of the Customer resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        $customers = Customer::select(['name', 'email', 'created_at']);

        return datatables($customers)
                ->editColumn('created_at', function ($customer) {
                    return $customer->created_at->format('d/m/Y');
                })
                ->make(true);
    }
}
