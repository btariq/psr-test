<?php

namespace App\Http\Controllers;

use Auth;
use App\Order as Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.index', ['title' => 'Orders Data', 'user' => Auth::user()]);
    }


    /**
     * Display a listing of the Order resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        $orders = Order::select(['id', 'invoice_number', 'total_amount', 'status', 'created_at']);

        return datatables($orders)
                ->editColumn('created_at', function ($order) {
                    return $order->created_at->format('d/m/Y');
                })
                ->make(true);
    }
}
